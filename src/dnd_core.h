#ifndef DND_CORE_H
#define DND_CORE_H

#include <vector>
#include <string>
#include <memory>

using namespace std;

namespace dnd {

    class Named {
    public:
        virtual ~Named() = default;
        virtual string getName() = 0;
    };

    enum CharacterAttributes {

        STRENGTH,
        INTELLIGENCE,
        PHISIQUE,
        AGILLITY,
        WISDOM,
        CAHRISMA,
        SPEED,
        INITIATIVE
    };

    enum ArmorClass {
        HEAVY,
        LIGHT,
        MEDIUM
    };

   /* class Modifiable {
    public:
        virtual ~Modifiable() = default;
        virtual CharacterAttributes* getModifiersCount(int& modifiersCount) = 0;
        virtual int getModifier(CharacterAttributes attribute) = 0;
    };

    class Equipment : public Modifiable, public Named  {
    public:
        virtual int getWeight() = 0;
    };

    class Armor : public Equipment {
    public:
        virtual int getArmorRatio() = 0;
        virtual ArmorClass getArmorClass() = 0;
    };

    class Weapon : public Equipment {
    public:
        virtual int getDamage() = 0;
    };

    class Buff : public Modifiable, public Named {};

    class Debuff: public Buff {
        const unique_ptr<Buff> buff;
    public:
        Debuff(Buff* buff);
        ~Debuff();
        CharacterAttributes* getModifiersCount(int& modifiersCount) = 0;
        int getModifier(CharacterAttributes attribute) = 0;
    };*/

    class Character : public Named {
    protected:
        const int* attributes;
        const int characterSize;

        int hp;
       /* Armor* currentArmor;
        Weapon* currentWeapon;
        vector<Buff> activeBuffs;
        vector<Equipment> inventar; */
    public:
        virtual ~Character() = default;
        int getAttributeValue(CharacterAttributes attribute);
        int getHP();
        /*void setArmor(Armor* armor);
        Armor* getArmor();
        void setWeapon(Weapon* weapon);
        Weapon* getWeapon();
        vector<Buff>* getActiveBuffsPointer();
        vector<Equipment>* getInventarPointer();*/
    };

    class Player : public Character {
    protected:
        const int maxWeight;
        int lvl;
    public:
        int getLevel();
        void setLevel();
        int getMaxWeight();
    };

    class Monstr : public Character {};

}

#endif // DND_CORE_H
