#ifndef DND_ENGINE_RENDER_H
#define DND_ENGINE_RENDER_H

namespace render {

    void onCreate(int width, int height);
    void onDraw();
    void onResize(int width, int height);
    void onClose();

}

#endif // DND_ENGINE_RENDER_H
