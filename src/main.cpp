#include <SFML/Window.hpp>
#include <cstdlib>

#include "render/render.h"
#include "dnd_core.h"

#define DEFAULT_WINDOW_WIDTH 1600
#define DEFAULT_WINDOW_HEIGHT 900

int main()
{
    sf::Window window;

    window.create(sf::VideoMode(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT), "D&D Engine");

    render::onCreate(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_WIDTH);

    while(window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Resized :
                {
                    sf::Vector2u windowSize = window.getSize();
                    render::onResize((int) windowSize.x, (int) windowSize.y);
                    break;
                }
            case sf::Event::Closed :
                window.close();
                break;
            }
            render::onDraw();
        }
    }

    render::onClose();

    return EXIT_SUCCESS;
}
